const defaultWatch = {edge:"rising", debounce:50, repeat:true};

var currentStyle = 0;
var batteryGraph = true;
var minutesRemaining = 0;

function centered(text, y) {
  g.drawString(text, (g.getWidth() - g.stringWidth(text))/2, y);
}

function showTimeAndBatt() {  
  let battery = Badge.getBatteryPercentage();
  g.clear();

  if (minutesRemaining > 0) {
    g.setFontVector(40);
    centered(minutesRemaining, 0);
    minutesRemaining--;
  } else {
    g.setFontVector(20);
    centered("Complete", 0);    
  }

  if (batteryGraph) {
    var x = battery / 100 * g.getWidth();  
    g.fillRect(0, g.getHeight() - 4, x, g.getHeight());
  } else {
    g.setFontBitmap();
    centered("Battery:" + battery + "%", g.getHeight() - 8);    
  }
  
  g.flip();
}

setWatch(function() {
  startCountdown(2);
}, BTNU, defaultWatch);

setWatch(function() {
  startCountdown(4);
}, BTNR, defaultWatch);

setWatch(function() {
  startCountdown(6);
}, BTND, defaultWatch);

setWatch(function() {
  startCountdown(12);
}, BTNL, defaultWatch);

function startCountdown(hours) {
  minutesRemaining = hours * 60;
  showTimeAndBatt();
}

setWatch(function() {
  batteryGraph = true;
  showTimeAndBatt();
}, BTNA, defaultWatch);

setWatch(function() {
  batteryGraph = false;
  showTimeAndBatt();
}, BTNB, defaultWatch);


setInterval(showTimeAndBatt, 60000);
showTimeAndBatt();
