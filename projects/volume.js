var controls = require("ble_hid_controls");
NRF.setServices(undefined, { hid : controls.report });


var clickTimer = null;
var clickCount = 0;

function doubleClick() {
  controls.volumeUp();  
  digitalPulse(LED1, 1, 100);
}

function click() {  
  controls.volumeDown();
  digitalPulse(LED2, 1, 100);
}

function clickTracking() {
    if (clickTimer !== null) {
        clearTimeout(clickTimer);
        clickTimer = null;
    }

    clickCount += 1;
  
    clickTimer = setTimeout(function () {
        clickTimer = null;
        if (clickCount == 1) {
          click();
        } else {
          doubleClick();
        }
        clickCount = 0;
    }, 400);
}

setWatch(clickTracking, BTN, {edge:"falling", debounce:50, repeat:true});
