const uart_service = '6e400001-b5a3-f393-e0a9-e50e24dcca9e';
const uart_tx = '6e400002-b5a3-f393-e0a9-e50e24dcca9e';
const uart_rx = '6e400003-b5a3-f393-e0a9-e50e24dcca9e';
const RED = LED1;
const GREEN = LED2;

const UP = '!B516';
const DOWN = '!B615';

const options = {
  timeout: 2000,
  filters: [
    {
      name: 'Magicbox',
      // services: ['6e400001b5a3f393e0a9e50e24dcca9e'],
    },
  ],
};

var busy = false;
var connected = false;
var txCharacteristic = false;
var open = true;

function sendToggle() {
  if (!busy) {
    busy = true;
    if (!connected) {
      NRF.requestDevice(options)
        .then(function(device) {
          return device.gatt.connect();
        })
        .then(function(d) {
          connected = d;
          return d.getPrimaryService(uart_service);
        })
        .then(function(s) {
          return s.getCharacteristic(uart_tx);
        })
        .then(function(c) {
          txCharacteristic = c;
          busy = false;
          // Now actually send the toggle command
          sendToggle();
        })
        .catch(function(e) {
          console.log('connect error', e);
          connected = false;
          digitalPulse(RED, 1, 500); // light red if we had a problem
          busy = false;
          if (connected) connected.disconnect();
        });
    } else {
      txCharacteristic
        .writeValue(open ? DOWN : UP)
        .then(function() {
          digitalPulse(GREEN, 1, 500); // light green to show it worked
          open = !open;
          busy = false;
        })
        .catch(function(e) {
          console.log('tx error', e);
          digitalPulse(RED, 1, 500); // light red if we had a problem
          busy = false;
        });
    }
  }
}

// Call sendToggle when the button is pressed
setWatch(sendToggle, BTN, {edge: 'rising', debounce: 50, repeat: true});
