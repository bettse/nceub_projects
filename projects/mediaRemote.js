
/* Ideas:
 * TZ
 * format
 * orientation
 * font
*/

const defaultWatch = {edge:"rising", debounce:50, repeat:true};

var controls = require("ble_hid_controls");
NRF.setServices(undefined, { hid : controls.report });

 
function centered(text, y) {
  g.drawString(text, (g.getWidth() - g.stringWidth(text))/2, y);
}

function log(msg) {
  console.log(msg);
  g.clear();
  g.setFontVector(14);
  centered(msg);
  g.flip();
}

function right() {
  log('right');
  //controls.next();
}

function left() {
  log('left');
  //controls.prev();
}

function up() {
  log('up');
  //controls.volumeUp();
}

function down() {
  log('down');
  //controls.volumeDown();
}

function A() {
  log('A');
  //controls.playpause();
}

function B() {
  log('B');
  //controls.mute();
}

setWatch(up, BTNU, defaultWatch);
setWatch(down, BTND, defaultWatch);
setWatch(left, BTNL, defaultWatch);
setWatch(right, BTNR, defaultWatch);
setWatch(A, BTNA, defaultWatch);
setWatch(B, BTNB, defaultWatch);
