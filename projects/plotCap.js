

function plotBadge(presses) {
  g.clear();    
  // function to work out each corner's coordinates
  function corner(c,r) {
    var a = (c-2)*Math.PI/3;
    return [64+Math.sin(a)*40*r, 32-Math.cos(a)*30*r];
  }
  // draw a hexagon - a line to each corner in turn
  g.moveTo.apply(g,corner(6,1));
  [1,2,3,4,5,6].forEach(c=>g.lineTo.apply(g,corner(c,1)));
  // Draw a circle where pressed
  presses.forEach((hit,c) => {
    if (!hit) return;
    var coords = corner(c+1,0.8);
    g.fillCircle(coords[0],coords[1],5);
  });
  g.flip();
}

function plotCap() {  
  var caps = [1,2,3,4,5,6].map(c=>Badge.capSense(c)>20000);
  plotBadge(caps);
}

setInterval(plotCap,50);