
const threshold = 1000;
const oneSecond = 1000;
const on = 1;
const RED = LED1;
const GREEN = LED2;
const BLUE = LED3;

//Baseline value, assume we're not near magnet when uploading code.
const zero = Puck.mag();

var doorOpen = false;

function onMag(p) {
  p.x -= zero.x;
  p.y -= zero.y;
  p.z -= zero.z;
  
  const s = Math.sqrt(p.x*p.x + p.y*p.y + p.z*p.z);
  const open = s < threshold;
  if (open != doorOpen) {
    doorOpen = open;
    digitalPulse(open ? RED : GREEN, on, oneSecond);
    NRF.setAdvertising({},{
      showName: true,
      manufacturer: 0x0590,
      manufacturerData: JSON.stringify({doorOpen})
    });
  }
}

Puck.on('mag', onMag);
Puck.magOn();