
/* Ideas:
 * TZ
 * format
 * orientation
 * font
*/

var strftime = require("strftime");
const defaultWatch = {edge:"rising", debounce:50, repeat:true};

E.setTimeZone(-8 /* PST */);

var currentStyle = 0;

var styles = [
  {
    formats: ['%r'],
    fontSize: 18
  },
  {
    formats: ['%R'],
    fontSize: 40
  },
  {
    formats: ['%T'],
    fontSize: 25
  },
  {
    formats: ['%b %d, %Y', '%T'],
    fontSize: 16
  }
];

function centered(text, y) {
  g.drawString(text, (g.getWidth() - g.stringWidth(text))/2, y);
}

function showTimeAndBatt() {  
  let battery = Badge.getBatteryPercentage();
  g.clear();
  const style = styles[currentStyle];
  
  g.setFontVector(style.fontSize);
  style.formats.forEach(function(format, index) {
    centered(strftime(format), index * style.fontSize);    
  });

  
  // Textually
  /*
  g.setFontBitmap();
  centered("Battery:" + battery + "%", g.getHeight() - 8);
  */
  
  // Graphically
  var x = battery / 100 * g.getWidth();  
  g.fillRect(0, g.getHeight() - 4, x, g.getHeight());
  
  g.flip();
}

function mod(num, mod) {
  var remain = num % mod;
  return Math.floor(remain >= 0 ? remain : remain + mod);
}

setWatch(function() {
  currentStyle = mod(currentStyle + 1, styles.length);
  showTimeAndBatt();
}, BTNR, defaultWatch);

setWatch(function() {
  currentStyle = mod(currentStyle - 1, styles.length);
  showTimeAndBatt();
}, BTNL, defaultWatch);

setInterval(showTimeAndBatt, 60000);
showTimeAndBatt();
